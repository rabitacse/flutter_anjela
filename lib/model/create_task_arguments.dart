import 'package:firebase_auth/firebase_auth.dart';

class CreateTaskArguments {
  FirebaseUser firebaseUser;
  CreateTaskArguments({this.firebaseUser});
}

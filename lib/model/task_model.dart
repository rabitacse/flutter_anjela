class TaskModel {
  String taskname;
  String taskdetails;
  String taskdate;
  String tasktime;
  String tasktype;
  bool isChecked;
  String uid;

  TaskModel(
      {this.taskname,
      this.taskdetails,
      this.taskdate,
      this.tasktime,
      this.tasktype,
      this.isChecked = false,
      this.uid});

  TaskModel.fromJson(Map<String, dynamic> json) {
    taskname = json['taskname'];
    taskdetails = json['taskdetails'];
    taskdate = json['taskdate'];
    tasktime = json['tasktime'];
    isChecked = json['isChecked'];
    tasktype = json['tasktype'];

    if (json['uid'] != null) {
      uid = json['uid'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['taskname'] = this.taskname;
    data['taskdetails'] = this.taskdetails;
    data['taskdate'] = this.taskdate;
    data['tasktime'] = this.tasktime;
    data['isChecked'] = this.isChecked;
    data['tasktype'] = this.tasktype;
    if (this.uid != null) {
      data['uid'] = this.uid;
    }
    return data;
  }

  @override
  String toString() {
    // TODO: implement toString
    return toJson().toString();
  }
}

import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter_anjela/page_routes.dart';
import 'package:flutter_anjela/pages/createTask.dart';
import 'package:flutter_anjela/pages/home_view.dart';
import 'package:flutter_anjela/pages/login.dart';
import 'package:flutter_anjela/pages/signup.dart';
import 'package:flutter_anjela/pages/splash_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: DevicePreview.appBuilder,
      debugShowCheckedModeBanner: false,
      home: PageRoutes(),
      title: 'Flutter Demo',
      routes: <String, WidgetBuilder>{
        '/landingpage': (BuildContext context) => SplashScreen(),
        '/signup': (BuildContext context) => new SignUpPage(),
        '/homeview': (BuildContext context) => new HomeView(),
        '/loginpage': (BuildContext context) => new LoginPage(),
        '/createtask': (BuildContext context) => new CreateTaskPage()
      },
    );
  }
}

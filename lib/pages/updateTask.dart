import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_anjela/model/task_model.dart';
import 'package:flutter_anjela/services/repository.dart';
import 'package:date_format/date_format.dart';
import 'package:intl/intl.dart';
import 'package:flutter_anjela/utils/screen_aware_size.dart';
import 'package:flutter_anjela/responsive/orientation_layout.dart';
import 'package:flutter_anjela/responsive/screen_type_layout.dart';
import 'package:flutter_anjela/pages/updateTaskView.dart';


class UpdateTask extends StatefulWidget {

  String taskID;
  final taskData;

  UpdateTask({this.taskID,this.taskData});

  @override
  _UpdateTaskState createState() => _UpdateTaskState();
}

class _UpdateTaskState extends State<UpdateTask> {


  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      mobile: OrientationLayout(
        portrait: UpdateTaskView(taskID:widget.taskID,taskData: widget.taskData),
      ),
    );
  }

}

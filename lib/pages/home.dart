import 'package:date_format/date_format.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_anjela/model/create_task_arguments.dart';
import 'package:flutter_anjela/services/repository.dart';
import 'package:flutter_shine/flutter_shine.dart';
import 'package:intl/intl.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String taskname;
  String taskdetails;
  DateTime taskdate;
  String tasktime;
  String tasktype;
  bool isChecked = true;
  final df = new DateFormat('dd-MM-yyyy');
  final f = new DateFormat('yyyy-MM-dd hh:mm');
  FirebaseUser firebaseUser;
  Repository repository = new Repository();
  GlobalKey<AnimatedListState> animatedListKey = GlobalKey<AnimatedListState>();

  var now = new DateTime.now();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCurrentUser();
  }

  getCurrentUser() async {
    firebaseUser = await repository.currentUser();
    setState(() {});
  }

  void toggleCheckbox(bool value) {
    if (isChecked == false) {
      setState(() {
        isChecked = true;
      });
    } else {
      setState(() {
        isChecked = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        child: BottomAppBar(
          //color: Colors.purple,
          shape: CircularNotchedRectangle(),
          child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
              const Color(0xFF4B0082),
              const Color(0xFF800080)
            ])),
            height: 75,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                  iconSize: 30.0,
                  padding: EdgeInsets.only(left: 28.0),
                  icon: Icon(Icons.home, color: Colors.white),
                  onPressed: () {
                    setState(() {
                      //_myPage.jumpToPage(0);
                    });
                  },
                ),
                IconButton(
                  iconSize: 30.0,
                  padding: EdgeInsets.only(right: 28.0),
                  icon: Icon(Icons.settings, color: Colors.white),
                  onPressed: () {
                    setState(() {
                      FirebaseAuth.instance.signOut().then((value) {
                        Navigator.of(context)
                            .pushReplacementNamed('/loginpage');
                      }).catchError((e) {
                        print(e);
                      });
                    });
                  },
                )
              ],
            ),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.purple[800],
        child: Icon(Icons.add),
        onPressed: () {
          //addDialog(context);
          Navigator.of(context).pushNamed('/createtask',
              arguments: CreateTaskArguments(firebaseUser: firebaseUser));
        },
      ),
      body: Column(
        children: <Widget>[
          Container(
            height: 220,
            width: double.infinity,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [const Color(0xFF4B0082), const Color(0xFF800080)]),
                borderRadius:
                    BorderRadius.only(bottomLeft: Radius.circular(50)),
                color: Colors.purple[800]),
            child: Container(
              margin: EdgeInsets.fromLTRB(20.0, 80.0, 0, 0),
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(10.0),
                    width: double.infinity,
                    margin: EdgeInsets.fromLTRB(0, 0, 200.0, 0),
                    child: Text(
                      formatDate(now, [d, '-', M, '-', yy]),
                      style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w400,
                          color: Colors.white),
                    ),
                  ),
                  SizedBox(height: 10),
                  FlutterShine(
                      builder: (BuildContext context, ShineShadow shineShadow) {
                    return Container(
                      margin: EdgeInsets.fromLTRB(0, 0, 260.0, 0),
                      child: Text(
                        'To Do List',
                        style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.w900,
                            color: Colors.white,
                            shadows: shineShadow?.shadows),
                      ),
                    );
                  }),
                ],
              ),
            ),
          ),

          //SizedBox(height: 200),

          Container(
            child: Expanded(
              child: _todoList(),
            ),
          )
        ],
      ),
    );
  }

  bool _anchorToBottom = true;
  Widget _todoList() {
    return firebaseUser != null
        ? FirebaseAnimatedList(
            physics: BouncingScrollPhysics(),
            key: new ValueKey<bool>(_anchorToBottom),
            query: repository.getPasswords(firebaseUser.uid),
//            sort: _anchorToBottom
//                ? (DataSnapshot a, DataSnapshot b) => a.value['taskname']
//                    .toString()
//                    .toUpperCase()
//                    .compareTo(b.value['taskname'].toString().toUpperCase())
//                : null,
            itemBuilder: (BuildContext context, DataSnapshot snapshot,
                Animation<double> animation, int index) {
              return new SizeTransition(
                sizeFactor: animation,
                child: todoItem(index, snapshot, context),
              );
            },
          )
        : Container();
  }

  Row todoItem(int i, DataSnapshot snapshot, BuildContext context) {
    return Row(
      children: <Widget>[
        Flexible(
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [const Color(0xFF4B0082), const Color(0xFF800080)]),
              borderRadius: BorderRadius.circular(20),
              // boxShadow:
            ),
            // color: Colors.purple,
            padding: EdgeInsets.all(4.0),
            margin: EdgeInsets.all(5.0),
            child: ListTile(
              trailing: IconButton(
                icon: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
                onPressed: () {
                  repository.deleteData(firebaseUser.uid, snapshot.key);

                  Scaffold.of(context).showSnackBar(
                      new SnackBar(content: new Text("List Removed")));
                },
              ),
              title: Row(
                children: <Widget>[
                  Checkbox(
                    value: snapshot.value['isChecked'],
                    onChanged: (value) {
                      print(value);
                      repository.updateCheckStatus(
                          value, firebaseUser.uid, snapshot.key);
                    },
                    activeColor: Colors.green,
                    checkColor: Colors.white,
                    tristate: false,
                  ),

//                  Checkbox(
//                    value: monVal,
//                    onChanged: (bool value) {
//                      setState(() {
//                        return new Dismissible(
//                          key: new Key(snapshot.key),
//                          onDismissed: (direction) {
//                            repository.deleteData(
//                                firebaseUser.uid, snapshot.key);
//                            Scaffold.of(context).showSnackBar(new SnackBar(
//                                content: new Text("Item Dismissed")));
//                          },
//                        );
//
////                        return repository.deleteData(firebaseUser.uid, snapshot.key);
//                      });
//                    },
//                  ),
                  Text(
                    snapshot.value['taskname'] ?? '',
                    style: TextStyle(
                        color: Colors.white,
                        decoration: snapshot.value['isChecked'] == true
                            ? TextDecoration.lineThrough
                            : TextDecoration.none),
                  ),
                ],
              ),
              subtitle: Text(
                snapshot.value['taskdate'] +
                        '  ' +
                        snapshot.value['tasktime'] ??
                    '',
                style: TextStyle(color: Colors.white),
              ),
              onTap: () {
//
//                return new Dismissible(
//                  key: new Key(snapshot.key),
//                  onDismissed: (direction) {
//                    repository.deleteData(firebaseUser.uid, snapshot.key);
//                    Scaffold.of(context).showSnackBar(
//                        new SnackBar(content: new Text("Item Dismissed")));
//                  },
//                );

//                Navigator.of(context).push(MaterialPageRoute(
//                    builder: (context) =>
//                        UpdateTask(taskID: snapshot.key, taskData: snapshot)));
              },
            ),
          ),
        )
      ],
    );
  }
}

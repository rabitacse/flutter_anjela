import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_anjela/model/create_task_arguments.dart';
import 'package:flutter_anjela/model/task_model.dart';
import 'package:flutter_anjela/services/repository.dart';
import 'package:intl/intl.dart';

class CreateTaskPage extends StatefulWidget {
  @override
  _CreateTaskPageState createState() => _CreateTaskPageState();
}

class _CreateTaskPageState extends State<CreateTaskPage> {
  String taskdate;
  String tasktime;
  String tasktype;
  DateTime taskTime;
  String _values = '';

  TaskModel model = new TaskModel();
  final format = DateFormat("yyyy-MM-dd");
  final timeformat = DateFormat("HH:mm");
  List<String> _locations = ['Fashion', 'Health', 'Meeitng', 'D']; // Option 2
  String _selectedLocation;
  final formKey = GlobalKey<FormState>();
  Repository repository = new Repository();
  CreateTaskArguments arguments;
  FirebaseUser firebaseUser;

  Future _selectDate() async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime.now().add(Duration(days: -365)),
        lastDate: new DateTime.now().add(Duration(days: 365)));

    if (picked != null) {
      setState(() {
        _values = picked.toString();
      });
    }
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    arguments = ModalRoute.of(context).settings.arguments;
    firebaseUser = arguments.firebaseUser;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
              const Color(0xFF4B0082),
              const Color(0xFF800080)
            ])),
          ),
          title: Text('Add Task'),
          backgroundColor: Colors.purple[800],
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).pop();
              })),
      body: SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height - 80,
                  child: ListView(
                    children: <Widget>[
                      textformfield('Task Title'),
                      textformfield('Task Details'),
                      buildTaskDate(),
                      buildTaskTime(),
                      buildTaskType(),
                      SizedBox(height: 20.0),
                      Padding(
                        padding: EdgeInsets.only(left: 16.0, right: 16.0),
                        child: Row(
                          children: <Widget>[
                            FlatButton(
                              child: Text(
                                'cancel',
                                style: TextStyle(color: Colors.white),
                              ),
                              color: Colors.red,
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                            SizedBox(width: 10.0),
                            FlatButton(
                              child: Text(
                                'Add',
                                style: TextStyle(color: Colors.white),
                              ),
                              color: Colors.purple[800],
                              onPressed: () async {
                                if (formKey.currentState.validate()) {
                                  formKey.currentState.save();
                                  var result = await repository.addData(
                                      model, firebaseUser.uid);
                                  Navigator.of(context).pop();
                                }
                                ;
                              },
                            ),
                          ],
                        ),
                      )
                    ],
                  ))
            ],
          ),
        ),
      ),
    );
  }

  Container buildTaskType() {
    return Container(
      padding: EdgeInsets.only(left: 16.0, right: 16.0),
      child: DropdownButtonFormField(
        validator: (value) {
          if (tasktype == null) {
            return 'Select Task Type';
          }
        },
        hint: Text('Please select type'),
        // Not necessary for Option 1
        value: tasktype,

        onChanged: (String values) {
          if (mounted) {
            setState(() {
              tasktype = values;
              model.tasktype = values;
            });
          }
        },
        items: _locations.map((location) {
          return DropdownMenuItem(
            child: new Text(location),
            value: location,
          );
        }).toList(),
      ),
    );
  }

  buildTaskTime() {
    return Padding(
      padding: EdgeInsets.only(left: 16.0, right: 16.0),
      child: DateTimeField(
        validator: (value) {
          if (tasktime == null) {
            return 'Select Task Time';
          }
        },
        format: timeformat,
        onShowPicker: (context, currentValue) async {
          final time = await showTimePicker(
            context: context,
            initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
          );
          return taskTime = DateTimeField.convert(time);
        },
        onChanged: (DateTime currentValue) {
          if (mounted) {
            setState(() {
              var time = DateFormat('HH:mm a').format(currentValue);
              tasktime = time.toString();
              model.tasktime = tasktime;
            });
          }
        },
        decoration: InputDecoration(
          labelText: "Task Time:",
        ),
      ),
    );
  }

  buildTaskDate() {
    return Padding(
      padding: EdgeInsets.only(left: 16.0, right: 16.0),
      child: DateTimeField(
        validator: (value) {
          print(taskdate);
          if (taskdate == null) {
            return 'Select Task Date';
          }
        },
        format: format,
        onShowPicker: (context, currentValue) {
          return showDatePicker(
              context: context,
              firstDate: DateTime(1900),
              initialDate: currentValue ?? DateTime.now(),
              lastDate: DateTime(2100));
        },
        onChanged: (DateTime currentValue) {
          taskTime = currentValue;
          var _dates = DateFormat('dd-MM-yyyy').format(currentValue);
          taskdate = _dates.toString();
          model.taskdate = taskdate;
        },
        decoration: InputDecoration(
          labelText: "Task Date:",
        ),
      ),
    );
  }

  Padding textformfield(label) {
    return Padding(
      padding: EdgeInsets.only(left: 16.0, right: 16.0),
      child: TextFormField(
        validator: (value) {
          if (value.isEmpty) {
            return '$label Can\'t Be Empty';
          }
        },
        onSaved: (value) {
          if (label == 'Task Title') {
            model.taskname = value;
          } else if (label == 'Task Details') {
            model.taskdetails = value;
          }
        },
        decoration: InputDecoration(
          labelText: "$label",
        ),
      ),
    );
  }
}

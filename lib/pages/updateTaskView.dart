import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_anjela/model/task_model.dart';
import 'package:flutter_anjela/services/repository.dart';
import 'package:date_format/date_format.dart';
import 'package:intl/intl.dart';
import 'package:flutter_anjela/utils/screen_aware_size.dart';
import 'package:flutter_anjela/responsive/orientation_layout.dart';
import 'package:flutter_anjela/responsive/screen_type_layout.dart';
//import 'package:tik_tok/views/home/home_view_mobile.dart';

class UpdateTaskView extends StatefulWidget {
  String taskID;
  final taskData;

  UpdateTaskView({this.taskID, this.taskData});

  @override
  _UpdateTaskViewState createState() => _UpdateTaskViewState();
}

class _UpdateTaskViewState extends State<UpdateTaskView> {
  String taskname;
  String taskdetails;
  String taskdate;
  String tasktime;
  DateTime taskTime;

  String tasktype;
  String _values = 'pick a date';
  bool _validate = false;
  final myController = TextEditingController(text: 'fdf');
  //final datesformat = DateFormat("yyyy-MM-dd");
  final df = new DateFormat('dd-MM-yyyy');
  final timeformat = DateFormat("HH:mm");
  List<String> _locations = ['Fashion', 'Health', 'Meeitng', 'D']; // Option 2
  String _selectedLocation;
  final _text = TextEditingController();
  TaskModel model = new TaskModel();

  Repository repository = new Repository();

  Future _selectDate() async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime.now().add(Duration(days: -365)),
        lastDate: new DateTime.now().add(Duration(days: 365)));

    if (picked != null) {
      setState(() {
        _values = picked.toString();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
          title: Text('Update Task'),
          backgroundColor: Colors.purple,
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).pushReplacementNamed('/homepage');
              })),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: ListView(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(screenAwareSize(10, context)),
                      child: TextFormField(
                        initialValue: widget.taskData['taskname'],
                        onChanged: (String value) {
                          taskname = value;
                        },
                        decoration: InputDecoration(
                          labelText: "Task Title:",
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(screenAwareSize(10, context)),
                      child: TextFormField(
                        // initialValue: widget.taskData['taskdetails'],
//                       controller: myController,
                        initialValue: 'ggg',

//                      onChanged: (String value) {
//                         taskdetails = value;
//                         },
                        decoration: InputDecoration(
                          labelText: "Task Details:",
                        ),
                        onSaved: (String value) {
                          taskname = 'dsdsd';
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(screenAwareSize(10, context)),
                      child: DateTimeField(
                        controller: new TextEditingController(
                            text: df
                                .format(new DateTime.fromMillisecondsSinceEpoch(
                                    widget.taskData['taskdate'].seconds * 1000))
                                .toString()),
                        format: df,
                        onShowPicker: (context, currentValue) {
                          return showDatePicker(
                              context: context,
                              firstDate: DateTime(1900),
                              initialDate: currentValue ?? DateTime.now(),
                              lastDate: DateTime(2100));
                        },
                        onChanged: (DateTime currentValue) {
                          taskdate = currentValue.toString();
                        },
                        decoration: InputDecoration(
                          labelText: "Task Date:",
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(screenAwareSize(10, context)),
                      child: DateTimeField(
                        controller: new TextEditingController(
                            text: timeformat.format(
                                new DateTime.fromMillisecondsSinceEpoch(
                                    widget.taskData['taskTime'].seconds *
                                        1000))),
                        format: timeformat,
                        onShowPicker: (context, currentValue) async {
                          final time = await showTimePicker(
                            context: context,
                            initialTime: TimeOfDay.fromDateTime(
                                currentValue ?? DateTime.now()),
                          );
                          return taskTime = DateTimeField.convert(time);
                        },
//                      onChanged: (DateTime currentValue) {
//                        if (mounted) {
//                          setState(() {
//                            tasktime = currentValue;
//                          });
//                        }
//                      },
                        decoration: InputDecoration(
                          labelText: "Task Time:",
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(screenAwareSize(10, context)),
                      child: DropdownButton(
                        hint: Text(widget.taskData[
                            'tasktype']), // Not necessary for Option 1
                        value: tasktype,

                        onChanged: (String values) {
                          if (mounted) {
                            setState(() {
                              tasktype = values;
                            });
                          }
                        },
                        items: _locations.map((location) {
                          return DropdownMenuItem(
                            child: new Text(location),
                            value: location,
                          );
                        }).toList(),
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Padding(
                      padding: EdgeInsets.all(screenAwareSize(10, context)),
                      child: Row(
                        children: <Widget>[
                          FlatButton(
                            child: Text(
                              'cancel',
                              style: TextStyle(color: Colors.white),
                            ),
                            color: Colors.red,
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                          SizedBox(width: 10.0),
                          FlatButton(
                            child: Text(
                              'Add',
                              style: TextStyle(color: Colors.white),
                            ),
                            color: Colors.blue,
                            onPressed: () async {
                              setState(() {
                                _text.text.isEmpty
                                    ? _validate = true
                                    : _validate = false;
                              });

                              var result = await repository.updateTaskData(
                                  TaskModel(
                                      taskname: taskname,
                                      taskdetails: taskdetails,
                                      taskdate: taskdate,
                                      tasktime: tasktime,
                                      tasktype: tasktype),
                                  widget.taskID,
                                  1);
                              if (result) {
                                Navigator.of(context)
                                    .pushReplacementNamed('/homepage');
                              } else {
                                print('FAILED');
                              }
                            },
                          ),
                        ],
                      ),
                    )
                  ],
                ))
          ],
        ),
      ),
    );
  }
}

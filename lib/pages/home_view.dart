import 'package:flutter/material.dart';
import 'package:flutter_anjela/pages/home.dart';
import 'package:flutter_anjela/responsive/orientation_layout.dart';
import 'package:flutter_anjela/responsive/screen_type_layout.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      mobile: OrientationLayout(
        portrait: HomePage(),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  String _name;
  String _email;
  String _password;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: Container(
//             margin: EdgeInsets.symmetric(vertical: 100.0,horizontal:5.0),
            padding: EdgeInsets.only(right: 20.0, top: 20.0),

            child: SingleChildScrollView(
              child: Column(
                //crossAxisAlignment: CrossAxisAlignment.baseline,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 320.0),
                    child: FlatButton(
                      onPressed: () {
                        Navigator.of(context)
                            .pushReplacementNamed('/loginpage');
                      },
                      child: Icon(
                        Icons.arrow_left,
                        color: Colors.black,
                        size: 50.0,
                      ),
                    ),
                  ),
                  SizedBox(height: 40),
                  Container(
                    margin: EdgeInsets.only(right: 200),
                    child: Text(
                      'Register',
                      style:
                          TextStyle(fontSize: 35, fontWeight: FontWeight.w800),
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    padding: EdgeInsets.fromLTRB(25, 20, 25, 25),
                    child: TextField(
                      onChanged: (value) {
                        setState(() {
                          _name = value;
                        });
                      },
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30.0)),
                          labelText: "Name"),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(25.0),
                    child: TextField(
                      keyboardType: TextInputType.emailAddress,
                      onChanged: (value) {
                        setState(() {
                          _email = value;
                        });
                      },
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30.0)),
                          labelText: "Email"),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(25, 20, 25, 25),
                    child: TextField(
                      obscureText: true,
                      onChanged: (value) {
                        setState(() {
                          _password = value;
                        });
                      },
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30.0)),
                          labelText: "Password"),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  RaisedButton(
                    onPressed: () {
                      FirebaseAuth.instance
                          .createUserWithEmailAndPassword(
                              email: _email, password: _password)
                          .then((AuthResult user) {})
                          .catchError((e) {
                        print(e);
                      });
                    },
                    textColor: Colors.white,
                    color: Colors.teal,
                    padding: const EdgeInsets.all(10.0),
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.teal)),
                    child: Container(
                      width: 300,
                      child: Center(
                        child: Text(
                          'Register',
                          style: TextStyle(fontSize: 20.0),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter_anjela/utils/screen_aware_size.dart';
import 'package:validators/validators.dart' as validator;

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String _email;
  String _password;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Container(
            padding: EdgeInsets.all(screenAwareSize(20, context)),
            height: MediaQuery.of(context).size.height,
            child: Column(
              //crossAxisAlignment: CrossAxisAlignment.baseline,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,

              children: <Widget>[
                SizedBox(height: screenAwareSize(100, context)),
                Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Text(
                    'Login',
                    style: TextStyle(
                        fontSize: screenAwareSize(35, context),
                        fontWeight: FontWeight.w800),
                  ),
                ),
                SizedBox(height: screenAwareSize(15, context)),
                Container(
                  margin: EdgeInsets.only(left: 10),
//                      child: Text(
//                        'Please sign in to continue',
//                        style: TextStyle(
//                            fontSize: screenAwareSize(18, context),
//                            fontWeight: FontWeight.bold),
//                      )
                  child: TypewriterAnimatedTextKit(
                    onTap: () {
                      print("Tap Event");
                    },
                    text: [
                      "Please sign-in to continue",
                    ],
                    textStyle: TextStyle(
                        fontSize: 18.0,
                        color: Colors.purple,
                        fontWeight: FontWeight.w800),
                  ),
                ),
                SizedBox(height: 15.0),
                _emailFormField(),
                SizedBox(height: 15.0),
                _passwordFormField(),
                SizedBox(height: 15.0),
                _signIn(),
                SizedBox(height: 10.0),
                _signUp()
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _emailFormField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      validator: (String value) {
        if (!validator.isEmail(value)) {
          return 'Please enter a Valid Email Address';
        } else {
          return null;
        }
      },
      decoration: InputDecoration(
          filled: true,
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(30.0)),
          labelText: "Email",
          contentPadding:
              EdgeInsets.only(left: 15.0, bottom: 0.0, top: 0.0, right: 0.0),
          // errorText: _validate ? 'Value Can\'t Be Empty' : null,
          errorStyle: TextStyle(
            fontSize: screenAwareSize(16, context),
          )),
      onChanged: (value) {
        setState(() {
          _email = value;
        });
      },
    );
  }

  Widget _passwordFormField() {
    return TextFormField(
      obscureText: true,
      validator: (String value) {
        if (value.length < 6) {
          return "Password should be min 6 characters";
        } else {
          return null;
        }
      },
      decoration: InputDecoration(
          filled: true,
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(30.0)),
          contentPadding:
              EdgeInsets.only(left: 20, top: 0, right: 0, bottom: 0),
          labelText: "Password",
          errorStyle: TextStyle(fontSize: screenAwareSize(16, context))),
      onChanged: (value) {
        setState(() {
          _password = value;
        });
      },
    );
  }

  Widget _signIn() {
    return RaisedButton(
      onPressed: () async {
//                _text.text.isEmpty ? _validate = true : _validate = false;

        if (_formKey.currentState.validate()) {
          FirebaseAuth.instance
              .signInWithEmailAndPassword(email: _email, password: _password)
              .then((AuthResult user) {
            //_isLoading=true;
            // _showCircularProgress();
            Navigator.of(context).pushReplacementNamed('/homepage');
          }).catchError((e) {
            print(e);
          });
        }
      },
      textColor: Colors.white,
      color: Colors.redAccent,
      padding: const EdgeInsets.all(10.0),
      shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(18.0),
          side: BorderSide(color: Colors.red)),
      child: Container(
        width: double.infinity,
        child: Center(
          child: Text(
            'Login',
            style: TextStyle(fontSize: screenAwareSize(20, context)),
          ),
        ),
      ),
    );
  }

  Widget _signUp() {
    return RaisedButton(
      onPressed: () {
        Navigator.of(context).pushReplacementNamed('/signup');
      },
      textColor: Colors.white,
      color: Colors.teal,
      padding: const EdgeInsets.all(10.0),
      shape: RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(18.0),
          side: BorderSide(color: Colors.teal)),
      child: Container(
        width: double.infinity,
        child: Center(
          child: Text(
            'Register',
            style: TextStyle(fontSize: screenAwareSize(20, context)),
          ),
        ),
      ),
    );
  }
}

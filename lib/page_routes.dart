import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_anjela/pages/home.dart';
import 'package:flutter_anjela/pages/home_view.dart';
import 'package:flutter_anjela/pages/login.dart';
import 'package:flutter_anjela/pages/splash_screen.dart';
import 'package:flutter_anjela/services/firebase_database.dart';

class PageRoutes extends StatefulWidget {
  @override
  _PageRoutesState createState() => _PageRoutesState();
}

FirebaseDatabaseHelper databaseUtil = new FirebaseDatabaseHelper();

class _PageRoutesState extends State<PageRoutes> {
  @override
  void initState() {
    super.initState();
    databaseUtil.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<FirebaseUser>(
        stream: FirebaseAuth.instance.onAuthStateChanged,
        builder: (BuildContext context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Loader();
          } else {
            if (snapshot.hasData) {
              return new SplashScreen();
            } else {
              return LoginPage();
            }
          }
        });
  }
}

//loader
class Loader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CircularProgressIndicator(),
    );
  }
}

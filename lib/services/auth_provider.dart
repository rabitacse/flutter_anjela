import 'package:firebase_auth/firebase_auth.dart';

abstract class BaseAuth {
  Future<FirebaseUser> currentUser();
}

class FirebaseAuthProvider implements BaseAuth {
  @override
  Future<FirebaseUser> currentUser() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();

    return user;
  }
}

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter_anjela/model/task_model.dart';

class FirebaseDatabaseHelper {
  DatabaseReference _databaseRef;
  FirebaseDatabase database = new FirebaseDatabase();
  DatabaseError error;

  static final FirebaseDatabaseHelper _instance =
      new FirebaseDatabaseHelper.internal();
  FirebaseDatabaseHelper.internal();

  factory FirebaseDatabaseHelper() {
    return _instance;
  }

  void initState() async {
    _databaseRef = database.reference().child('todolist');
    await database.setPersistenceEnabled(true);
  }

  Query getPasswords(String uid) {
    return _databaseRef.child(uid);
  }

  //adding task
  Future<bool> addTask(TaskModel model, String uid) async {
    var result = await _databaseRef.child(uid).push().set(model.toJson());
    return true;
  }

  // update task
  updateTask(TaskModel model, String uid, id) {
    var res = _databaseRef.child(uid).child(id).update(model.toJson());
    return res;
  }

  updateCheckStatus(bool isChecked, String uid, id) {
    print(id);
    var res =
        _databaseRef.child(uid).child(id).update({'isChecked': isChecked});
    return res;
  }

//  //deleting task
  deleteTask(String uid, id) async {
    await _databaseRef.child(uid).child(id).remove().then((_) {
      print('Transaction  Deleted.');
    });
  }
}

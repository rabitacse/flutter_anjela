import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter_anjela/services/auth_provider.dart';
import 'package:flutter_anjela/services/firebase_database.dart';

class Repository {
  FirebaseDatabaseHelper _firebaseDatabaseHelper = new FirebaseDatabaseHelper();

  FirebaseAuthProvider _authProvider = new FirebaseAuthProvider();

  Future<bool> addData(taskData, uid) async {
    print(taskData.toString());
    print(uid);
    return await _firebaseDatabaseHelper.addTask(taskData, uid);
  }

  Query getPasswords(String uid) {
    return _firebaseDatabaseHelper.getPasswords(uid);
  }

  updateTaskData(taskData, uid, id) {
    return _firebaseDatabaseHelper.updateTask(taskData, uid, id);
  }

  updateCheckStatus(bool isChecked, String uid, id) {
    return _firebaseDatabaseHelper.updateCheckStatus(isChecked, uid, id);
  }

  deleteData(uid, id) {
    return _firebaseDatabaseHelper.deleteTask(uid, id);
  }

  Future<FirebaseUser> currentUser() async {
    return await _authProvider.currentUser();
  }
}
